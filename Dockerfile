# Creates docker container that runs dcm2niix (modified from scitran/dcm2niix)
#
# Example usage:
#   docker run --rm -ti \
#       -v <someDirWithDicoms>:/input \
#       -v <emptyOutputFolder>:/output \
#       registry.gitlab.com/blakedewey/dcm2niix-app
ARG BASE_DISTRO=ubuntu
ARG BASE_VERSION=20.04
ARG PACKAGE_MANAGER=apt
ARG DCM2NIIX_VERSION=1.0.20211006
ARG GDCM_VERSION=3.0.12
ARG N_BUILD_THREADS=4


FROM registry.gitlab.com/neurobuilds/dcm2niix:${DCM2NIIX_VERSION}-${BASE_DISTRO}${BASE_VERSION} as dcm2niix


FROM ${BASE_DISTRO}:${BASE_VERSION} as apt-base
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq && \
    apt-get install -y -q \
           wget \
           gcc \
           g++ \
           cmake \
           git


FROM ${BASE_DISTRO}:${BASE_VERSION} as yum-base
ARG N_BUILD_THREADS

RUN yum install -y -q \
           wget \
           gcc-c++ \
           make \
           git \
           openssl-devel 

RUN cd /opt && \
    curl -L --silent https://cmake.org/files/v3.20/cmake-3.20.4.tar.gz > cmake-3.20.4.tar.gz && \
    tar -xzf cmake-3.20.4.tar.gz && \
    cd cmake-3.20.4 && \
    ./bootstrap --parallel=${N_BUILD_THREADS} && \
    make -j${N_BUILD_THREADS} && \
    make install


FROM ${PACKAGE_MANAGER}-base as install-gdcm
ARG GDCM_VERSION
ARG N_BUILD_THREADS

RUN mkdir -p /tmp/gdcm-src && \
    cd /tmp/gdcm-src && \
	wget https://github.com/malaterre/GDCM/archive/refs/tags/v${GDCM_VERSION}.tar.gz && \
	tar -xzf v${GDCM_VERSION}.tar.gz && \
	cd GDCM-${GDCM_VERSION} && \
	mkdir -p build && \
	cd build && \
	cmake \
	    -DCMAKE_INSTALL_PREFIX=/opt/gdcm \
	    -DGDCM_BUILD_APPLICATIONS=ON \
	    .. && \
	make -j${N_BUILD_THREADS} && \
	make install

RUN wget https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 && \
    mv jq-linux64 /usr/bin/jq && \
    chmod +x /usr/bin/jq

RUN echo -e "{\
    \n  \"build_version\": \"$GDCM_VERSION\" \
    \n}" > /opt/gdcm/manifest.json


FROM ${BASE_DISTRO}:${BASE_VERSION} as final
LABEL maintainer=blake.dewey@jhu.edu

# Copy Build Artifacts
COPY --from=dcm2niix /opt/dcm2niix /opt/dcm2niix
COPY --from=install-gdcm /opt/gdcm/bin/gdcmconv /opt/dcm2niix/bin
COPY --from=install-gdcm /usr/bin/jq /opt/dcm2niix/bin

ENV PATH /opt/dcm2niix/bin:${PATH}

# Add manifest and script
COPY manifest.json run_dcm2niix.sh /opt

# Configure entrypoint
ENTRYPOINT ["/bin/bash", "/opt/run_dcm2niix.sh"]
