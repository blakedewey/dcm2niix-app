This Docker app creates NIfTI image files from DICOM images. Dicom images can be zipped, tarballed or in a directory.

Usage: `docker run --rm -v /path/to/dicoms:/input -v /path/to/output:/output registry.gitlab.com/blakedewey/dcm2niix-app`

The first -v option maps the directory on the host machine (containing DICOMs or a zipped collection of DICOMs), which is then mapped into the container at /input. The second -v option maps a directory on the host machine for the output NIfTI images.

LICENSE: This container contains software that is licensed according to the original software license (see https://github.com/rordenlab/dcm2niix).
